import argparse
import hashlib
parser = argparse.ArgumentParser() #instanciación del objeto
parser.add_argument('name', help="name of the user")
parser.add_argument('last_name', help="last_name")
parser.add_argument("-p", '--print', help="imprime una cadena", action='store_true', default="Hola")
args = parser.parse_args()
hashname= hashlib.md5(args.name.encode()).hexdigest()
print("{0} arg.print, {1} arg.name, hashname {2}, {3} last_name ".format(args.print, args.name, hashname, args.last_name))

